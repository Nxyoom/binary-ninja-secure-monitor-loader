# Binary Ninja Secure Monitor Loader

Author: **EliseZeroTwo**

_NX Secure Monitor Loader (FW 3.0.2) for Binary Ninja._

Features:

* Maps binary
* Maps MMIO
* Maps memory
* Applies relocations
* Labels functions

## Minimum Version

This plugin requires the following minimum version of Binary Ninja:

* 3000

## License

This plugin is released under [MIT](./license).

## Metadata Version

2
