from os import name
from binaryninja.architecture import Architecture
from binaryninja.binaryview import BinaryReader, BinaryView, BinaryWriter
from binaryninja.enums import Endianness, SectionSemantics, SegmentFlag, SymbolType
from binaryninja.log import log_error, log_info
from binaryninja.types import Symbol
from binaryninja import AnalysisCompletionEvent

BASE = 0x4002D000

MAPPINGS = [
    ("iRAM-A", 0x40000000, 0x10000, SegmentFlag.SegmentExecutable | SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable | SegmentFlag.SegmentContainsCode | SegmentFlag.SegmentContainsData),
    ("iRAM-B", 0x40010000, 0x10000, SegmentFlag.SegmentExecutable | SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable | SegmentFlag.SegmentContainsCode | SegmentFlag.SegmentContainsData),
    ("iRAM-D", 0x40030000, 0x10000, SegmentFlag.SegmentExecutable | SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable | SegmentFlag.SegmentContainsCode | SegmentFlag.SegmentContainsData),
    ("TZRAM-L", 0x7c010000, 0x3000, SegmentFlag.SegmentExecutable | SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable | SegmentFlag.SegmentContainsCode | SegmentFlag.SegmentContainsData),
    ("TZRAM-H", 0x7c013500, 0x10000 - 0x3500, SegmentFlag.SegmentExecutable | SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable | SegmentFlag.SegmentContainsCode | SegmentFlag.SegmentContainsData),
    ("TMR_AND_WATCHDOG", 0x60005000, 0x1000, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("SYSTEM_REGISTERS", 0x6000C000, 0x300, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("APB_DMA", 0x60020000, 0x4000, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("MSELECT", 0x50060000, 0x1000, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("SYSCTR0", 0x700F0000, 0x10000, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("HOST1X", 0x50000000, 0x40000, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("I2C", 0x7000c000, 0x100, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("I2C2", 0x7000c400, 0x100, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("I2C3", 0x7000c500, 0x100, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("I2C4", 0x7000c700, 0x100, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("I2C5", 0x7000d000, 0x100, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("I2C6", 0x7000d100, 0x100, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("RTC", 0x7000e000, 0x100, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("PMC", 0x7000e400, 0xc00, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("FUSE", 0x7000f800, 0x400, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("KFUSE", 0x7000fc00, 0x400, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("SE", 0x70012000, 0x2000, SegmentFlag.SegmentReadable |
     SegmentFlag.SegmentWritable),
    ("MC", 0x70019000, 0x1000, SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable)
]

DATA = {
    # (name, addr, ty)
    ("LVL1_TRANSLATION_TABLE", 0x7c0107c0, "uint64_t[0x8]"),
    ("LVL2_TRANSLATION_TABLE", 0x7c011000, "uint64_t[0x200]"),
    ("LVL3_TRANSLATION_TABLE", 0x7c012000, "uint64_t[0x200]"),

    ("OHAYO", 0x1f01e7530, "char[0x7]"),
    ("OYASUMI", 0x1f01e7537, "char[0x9]"),

    ("CLOCK_AND_RESET_MMIO_ADDRESS", 0x1f01e7580, "uint32_t *"),
    ("FLOW_CONTROLLER_MMIO_ADDRESS", 0x1f01e7588, "uint32_t *"),
    ("FUSE_MMIO_ADDRESS", 0x1f01e7590, "uint32_t *"),
    ("APB_MMIO_ADDRESS", 0x1f01e75d8, "uint32_t *"),
    ("GPIO_MMIO_ADDRESS", 0x1f01e75e0, "uint32_t *"),
    ("PMC_MMIO_ADDRESS", 0x1f01e75e8, "uint32_t *"),
    ("UART_MMIO_ADDRESS", 0x1f01e75f0, "uint32_t *"),
    ("TMR_MMIO_ADDRESS_1", 0x1f01e75f8, "uint32_t *"),
    ("TMR_MMIO_ADDRESS_2", 0x1f01e7600, "uint32_t *"),
    ("SE_MMIO_ADDRESS", 0x1f01e8940, "uint32_t *"),
    ("I2C_1_TO_4_MMIO_ADDRESS", 0x1f01e75a8, "uint32_t *"),
    ("I2C_5_TO_6_MMIO_ADDRESS", 0x1f01e75c8, "uint32_t *"),

    ("INTERRUPT_DISTRIBUTOR_MMIO_ADDRESS", 0x1f01e7598, "uint32_t *"),
    ("INTERRUPT_CONTROLLER_PCPU_IFACE_MMIO_ADDRESS", 0x1f01e75a0, "uint32_t *"),

    ("V_PMC_MMIO", 0x1f0089400, "void"),
    ("V_FUSE_MMIO", 0x1f0096800, "void"),

    ("PKG2", 0xa9800000, "void"),
    ("RSA_CONTEXTS", 0x1f01e8950, "struct RsaCtx[2]"),
    ("PKG2_DEV_MODULUS", 0x1f01e7718, "uint8_t[0x100]"),
    ("PKG2_PROD_MODULUS", 0x1f01e7818, "uint8_t[0x100]"),
    ("CAR_DEVICES", 0x1f01e7270, "struct CARDeviceDefinition[0x8]")
}

VIRTUAL_MAPPINGS = {
    #(Name, source_start, source_len, va_dest_start, va_dest_len, flags)

    # Code
    "code": {
        ("RELOC_0_TZRAM", 0x4002f0f8, 0x4f4, 0x7c013000, 0x500, SegmentFlag.SegmentExecutable |
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("V_RELOC_0_TZRAM", 0x4002f0f8, 0x4f4, 0x1f01e0000, 0xB000, SegmentFlag.SegmentExecutable |
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("RELOC_1", 0x4002dac8, 0xe20, 0x1f01f0000, 0x2000, SegmentFlag.SegmentExecutable |
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("VECTOR_TABLE", 0x4002e900, 0x7f8, 0x1f01fa800, 0x800, SegmentFlag.SegmentExecutable |
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("RELOC_2", 0x4002f600, 0x799c, 0x1f01e0500, 0x7A00, SegmentFlag.SegmentExecutable |
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
    },

    # MMIO
    "mmio": {
        ("INTERRUPT_DISPATCHER", 0x00, 0x00, 0x1f0080000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("INTERRUPT_CONTROLLER_PCPU", 0x00, 0x00, 0x1f0082000, 0x2000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("CLOCK_AND_RESET", 0x00, 0x00, 0x1f0087000, 0x2000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("TMR_AND_WATCHDOG", 0x00, 0x00, 0x1f008b000, 0x2000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("SYSREGS_AHB_ARB_GIZMO_AHB_APB_DEBUG_SECBOOT_ACMON", 0x00, 0x00, 0x1f008d000,
         0x1000, SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("FLOW_CTRL_AHB_DMA", 0x00, 0x00, 0x1f009d000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("EXCEPTION_VECTORS", 0x00, 0x00, 0x1f00a7000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("RTC", 0x00, 0x00, 0x1f0089000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("UART", 0x00, 0x00, 0x1f0085000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("SE", 0x00, 0x00, 0x1f008f000, 0x2000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("MC", 0x00, 0x00, 0x1f0094000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("SYSCTR0", 0x00, 0x00, 0x1f0092000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("FUSE_AND_KFUSE", 0x00, 0x00, 0x1f0096000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("APB_BASE", 0x00, 0x00, 0x1f0098000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("I2C_1_TO_4", 0x00, 0x00, 0x1f00a1000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        ("I2C_5_TO_6", 0x00, 0x00, 0x1f00a5000, 0x1000,
         SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable),
        # There are other things we should add here but, later
    },

    # Data
    "data": {
        ("LVL2_TRANSLATION_TABLE", 0x1f01fc000, 0x1000),
        ("LVL3_TRANSLATION_TABLE", 0x1f01fe000, 0x1000),
    }

}

FUNCTIONS = [
    #(Name, Address)
    ("_start", BASE, ("void", [], True)),
    ("init_cold", 0x4002d1a8, ("void", [])),
    ("memcpy", 0x4002d35c, ("void", [
     "void *dest", "const void *src", "uint64_t count"])),
    ("memclear", 0x4002d3b0, ("void", ["void *data", "uint64_t count"])),
    ("map_lvl1_dtable", 0x4002d400, ("void", [
     "uint64_t *lvl1_translation_table", "uint64_t* mapped_block"])),
    ("map_lvl2_dtable", 0x4002d414, ("void", [
     "uint64_t *lvl2_translation_table", "uint64_t* mapped_block"])),
    ("map_lvl1_blocks", 0x4002d428,
     ("void", ["uint64_t *lvl1_translation_table"])),
    ("map_lvl3_address", 0x4002d45c, ("void", [
     "uint64_t *lvl3_translation_table", "void *vaddr", "void *paddr", "uint64_t size", "uint64_t attributes"])),
    ("configure_translation_table", 0x4002d49c, ("void", [])),
    ("align_down", 0x4002d9f4, ("uint64_t", [
     "uint64_t value", "uint64_t alignment"])),
    ("align_up", 0x4002da00, ("uint64_t", [
     "uint64_t value", "uint64_t alignment"])),
    ("configure_and_enable_mmu", 0x4002da38, ("void", [])),

    ("vt_handle_synchronous_interrupt_from_current_el_using_sp_el0",
     0x4002e900, ("void", [])),
    ("vt_generic_handler", 0x4002e904, ("void", [])),
    ("vt_handle_irq_from_current_el_using_sp_el0", 0x4002e980, ("void", [])),
    ("vt_handle_fiq_from_current_el_using_sp_el0", 0x4002ea00, ("void", [])),
    ("vt_handle_serror_from_current_el_using_sp_el0", 0x4002ea80, ("void", [])),

    ("vt_handle_synchronous_interrupt_from_current_el_using_sp_elx",
     0x4002eb00, ("void", [])),
    ("vt_handle_irq_from_current_el_using_sp_elx", 0x4002eb80, ("void", [])),
    ("vt_handle_fiq_from_current_el_using_sp_elx", 0x4002ec00, ("void", [])),
    ("vt_handle_serror_from_current_el_using_sp_elx", 0x4002ec80, ("void", [])),

    ("vt_handle_synchronous_interrupt_from_lower_el_a64", 0x4002ed00, ("void", [])),
    ("vt_handle_irq_from_lower_el_a64", 0x4002ed80, ("void", [])),
    ("vt_handle_fiq_from_lower_el_a64", 0x4002ee00, ("void", [])),
    ("vt_handle_serror_from_lower_el_a64", 0x4002ee80, ("void", [])),

    ("vt_handle_synchronous_interrupt_from_lower_el_a32", 0x4002ef00, ("void", [])),
    ("vt_handle_irq_from_lower_el_a32", 0x4002ef80, ("void", [])),
    ("vt_handle_fiq_from_lower_el_a32", 0x4002f000, ("void", [])),
    ("vt_handle_serror_from_lower_el_a32", 0x4002f080, ("void", [])),

    ("drop_to_el1", 0x4002f600, ("void", [], True)),
    ("get_stack_start_by_cpuid", 0x4002f6a0, ("void *", [])),

    ("invalidate_unified_and_data_caches", 0x7c013324, ("void", [])),

    ("relocated_drop_to_el1", 0x1f01e0500, ("void", [], True)),
    ("relocated_get_stack_start_by_cpuid", 0x1f01e05a0, ("void *", [])),

    ("relocated_full_system_reset_via_watchdog",
     0x1f01e3250, ("void", ["uint32_t *tmr_base"], True)),
    ("relocated_call_full_system_reset_via_watchdog_with_mapped_tmr",
     0x1f01e32a0, ("void", [], True)),

    ("relocated_vt_handle_synchronous_interrupt_from_current_el_using_sp_el0",
     0x1f01fa800, ("void", [])),
    ("relocated_vt_generic_handler", 0x1f01fa804, ("void", [])),
    ("relocated_vt_handle_irq_from_current_el_using_sp_el0", 0x1f01fa880, ("void", [])),
    ("relocated_vt_handle_fiq_from_current_el_using_sp_el0", 0x1f01fa900, ("void", [])),
    ("relocated_vt_handle_serror_from_current_el_using_sp_el0",
     0x1f01fa980, ("void", [])),

    ("relocated_vt_handle_synchronous_interrupt_from_current_el_using_sp_elx",
     0x1f01faa00, ("void", [])),
    ("relocated_vt_handle_irq_from_current_el_using_sp_elx", 0x1f01faa80, ("void", [])),
    ("relocated_vt_handle_fiq_from_current_el_using_sp_elx", 0x1f01fab00, ("void", [])),
    ("relocated_vt_handle_serror_from_current_el_using_sp_elx",
     0x1f01fab80, ("void", [])),

    ("relocated_vt_handle_synchronous_interrupt_from_lower_el_a64",
     0x1f01fac00, ("void", [])),
    ("relocated_vt_handle_irq_from_lower_el_a64", 0x1f01fac80, ("void", [])),
    ("relocated_vt_handle_fiq_from_lower_el_a64", 0x1f01fad00, ("void", [])),
    ("relocated_vt_handle_serror_from_lower_el_a64", 0x1f01fad80, ("void", [])),

    ("relocated_vt_handle_synchronous_interrupt_from_lower_el_a32",
     0x1f01fae00, ("void", [])),
    ("relocated_vt_handle_irq_from_lower_el_a32", 0x1f01fae80, ("void", [])),
    ("relocated_vt_handle_fiq_from_lower_el_a32", 0x1f01faf00, ("void", [])),
    ("relocated_vt_handle_serror_from_lower_el_a32", 0x1f01faf80, ("void", [])),

    ("relocated_invalid_smc", 0x1f01e4afc, ("void", [], True)),
    ("relocated_decode_smc", 0x1f01e600c, ("void", [
     "uint32_t smc_imm_val", "PushedRegisters *arguments"])),
    ("relocated_handle_smc", 0x1f01faf84, ("void", [])),

    ("set_pmc_scratch_200_if_currently_clear",
     0x1f01e61a4, ("void", ["uint32_t value"])),
    ("panic", 0x1f01e32fc, ("void", [], True)),
    ("lock_private_key_fuses", 0x1f01e3654, ("void", [])),
    ("lock_se", 0x1f01e29cc, ("void", [])),

    ("set_fuse_mmio_base", 0x1f01e352c, ("void", ["uint32_t *new_base"])),
    ("set_clock_and_reset_mmio_base", 0x1f01e2be4,
     ("void", ["uint32_t *new_base"])),
    ("set_flow_controller_mmio_base", 0x1f01e0b8c,
     ("void", ["uint32_t *new_base"])),
    ("set_interrupt_distributor_and_interrupt_controller_mmio_base", 0x1f01e2d90, ("void", [
     "uint32_t *new_interrupt_distributor_base", "uint32_t *new_interrupt_controller_base"])),
    ("set_apb_and_gpio_mmio_base", 0x1f01e6ce0, ("void", [
     "uint32_t *new_apb_base", "uint32_t *new_gpio_base"])),
    ("set_pmc_mmio_base", 0x1f01e3188, ("void", ["uint32_t *new_base"])),
    ("set_uart_mmio_base", 0x1f01e673c, ("void", ["uint32_t *new_base"])),
    ("set_tmr_mmio_base_1", 0x1f01e681c, ("void", ["uint32_t *new_base"])),
    ("set_tmr_mmio_base_2", 0x1f01e3244, ("void", ["uint32_t *new_base"])),
    ("set_se_mmio_base", 0x1f01e2398, ("void", ["uint32_t *new_base"])),
    ("set_i2c_mmio_bases", 0x1f01e3178,
     ("void", ["uint32_t idx", "uint32_t *new_base"])),


    ("relocated_memcpy_where_size_is_4_byte_aligned", 0x1f01e0908,
     ("void", ["uint32_t *dst", "const uint32_t *src", "uint64_t length"])),
    ("relocated_memcpy_impl", 0x1f01e097c, ("void", [
     "uint8_t *dst", "const uint8_t *src", "uint64_t length"])),
    ("relocated_memcpy", 0x1f01e0898, ("void", [
     "uint8_t *dst", "const uint8_t *src", "uint64_t length"])),
    ("relocated_memclr_0x190_bytes", 0x1f01e6ba4, ("void", ["uint8_t *dst"])),
    ("relocated_fast_memset", 0x1f01e08c4, ("void", [
     "uint8_t *dst", "uint8_t val", "uint64_t length"])),
    ("relocated_fast_memset_impl", 0x1f01e0a88, ("void", [
     "uint8_t *dst", "uint64_t length", "uint32_t val"])),
    ("relocated_fast_memset_impl_2", 0x1f01e0a50,
     ("void", ["uint8_t *dst", "uint64_t length", "uint32_t val"])),

    ("clear_and_invalidate_cache_for_address_range",
     0x1f01e0bdc, ("void", ["void *start", "uint64_t length"])),

    ("check_if_dev_or_prod_or_invalid", 0x1f01e34fc, ["uint64_t", []]),
    ("is_prod", 0x1f01e32b4, ["bool", []]),
    ("is_prod_2", 0x1f01e32e0, ["bool", []]),

    ("read_tmrus_cntr_us", 0x1f01e67ec, ["uint32_t", []]),
    ("sleep_us", 0x1f01e67fc, ["void", ["uint32_t us"]]),

    ("panic_if_se_isnt_idle", 0x1f01e0c00, ["void", []]),
    ("get_se_mmio_base", 0x1f01e2370, ["uint32_t *", []]),
    ("se_panic_if_operation_failed", 0x1f01e265c, ["void", []]),
    ("se_copy_rsa_output", 0x1f01e16dc, [
     "void", ["uint8_t *output", "uint64_t output_length"]]),
    ("se_do_sha256", 0x1f01e1ec8, [
     "void", ["uint8_t *output", "uint8_t *input", "uint64_t size"]]),
    ("se_do_operation", 0x1f01e0c24, ["void", [
     "uint32_t se_int_enable", "void* output", "uint32_t output_size", "void* input", "uint32_t input_size"]]),
    ("se_read_rsa_output", 0x1f01e16dc, [
     "void", ["char *rsa_output", "uint64_t output_len"]]),
    ("enable_clock_bring_out_of_reset", 0x1f01e2adc,  [
     "void", ["struct CARDeviceDefinition* device"]]),
]

REGISTERS = [
    # (name, address, type)
    ("MSELECT_CONFIG_0", 0x50060000, "uint32_t"),
    ("SYSCTR0_CNTCR_0", 0x700f0000, "uint32_t"),
    ("AHB_MASTER_SWID_0", 0x6000c018, "uint32_t"),
    ("AHB_MASTER_SWID_1", 0x6000c038, "uint32_t"),
    ("APBDMA_CHANNEL_SWID_0", 0x6002003c, "uint32_t"),
    ("APBDMA_CHANNEL_SWID1_0", 0x60020054, "uint32_t"),
    ("APBDMA_SECURITY_REG_0", 0x60020038, "uint32_t"),
    ("AHB_ARBITRATION_DISABLE_0", 0x6000c004, "uint32_t"),
    ("AHB_ARBITRATION_PRIORITY_CTRL_0", 0x6000c008, "uint32_t"),
    ("AHB_ARBITRATION_PRIORITY_CTRL_0", 0x6000c008, "uint32_t"),
    ("AHB_GIZMO_TZRAM_0", 0x6000c054, "uint32_t"),

    ("BOOT_STATE", 0x1f009fef8, "uint32_t"),
    ("SECMON_BOOT_STATE", 0x1f009fefc, "uint32_t"),

    ("SE_SECURITY_CONTROL", 0x1f008f000, "uint32_t"),
    ("SE_TZRAM_SECURITY", 0x1f008f004, "uint32_t"),
    ("SE_OPERATION", 0x1f008f008, "uint32_t"),
    ("SE_INT_ENABLE", 0x1f008f00c, "uint32_t"),
    ("SE_INT_STATUS", 0x1f008f010, "uint32_t"),
    ("SE_CONFIG", 0x1f008f014, "uint32_t"),
    ("SE_IN_LL_ADDR", 0x1f008f018, "uint32_t"),
    ("SE_IN_CUR_BYTE_ADDR", 0x1f008f01c, "uint32_t"),
    ("SE_OUT_LL_ADDR", 0x1f008f020, "uint32_t"),
    ("SE_OUT_CUR_BYTE_ADDR", 0x1f008f024, "uint32_t"),
    ("SE_OUT_CUR_LL_ID", 0x1f008f02c, "uint32_t"),
    ("SE_HASH_RESULT_0", 0x1f008f030, "uint32_t"),
    ("SE_HASH_RESULT_1", 0x1f008f034, "uint32_t"),
    ("SE_HASH_RESULT_2", 0x1f008f038, "uint32_t"),
    ("SE_HASH_RESULT_3", 0x1f008f03c, "uint32_t"),
    ("SE_HASH_RESULT_4", 0x1f008f040, "uint32_t"),
    ("SE_HASH_RESULT_5", 0x1f008f044, "uint32_t"),
    ("SE_HASH_RESULT_6", 0x1f008f048, "uint32_t"),
    ("SE_HASH_RESULT_7", 0x1f008f04c, "uint32_t"),
    ("SE_HASH_RESULT_8", 0x1f008f050, "uint32_t"),
    ("SE_HASH_RESULT_9", 0x1f008f054, "uint32_t"),
    ("SE_HASH_RESULT_10", 0x1f008f058, "uint32_t"),
    ("SE_HASH_RESULT_11", 0x1f008f05c, "uint32_t"),
    ("SE_HASH_RESULT_12", 0x1f008f060, "uint32_t"),
    ("SE_HASH_RESULT_13", 0x1f008f064, "uint32_t"),
    ("SE_HASH_RESULT_14", 0x1f008f068, "uint32_t"),
    ("SE_HASH_RESULT_15", 0x1f008f06c, "uint32_t"),
    ("SE_CTX_SAVE_CONFIG", 0x1f008f070, "uint32_t"),
    ("SE_SHA_CONFIG", 0x1f008f200, "uint32_t"),
    ("SE_SHA_MSG_LENGTH_0", 0x1f008f204, "uint32_t"),
    ("SE_SHA_MSG_LENGTH_1", 0x1f008f208, "uint32_t"),
    ("SE_SHA_MSG_LENGTH_2", 0x1f008f20c, "uint32_t"),
    ("SE_SHA_MSG_LENGTH_3", 0x1f008f210, "uint32_t"),
    ("SE_SHA_MSG_LEFT_0", 0x1f008f214, "uint32_t"),
    ("SE_SHA_MSG_LEFT_1", 0x1f008f218, "uint32_t"),
    ("SE_SHA_MSG_LEFT_2", 0x1f008f21c, "uint32_t"),
    ("SE_SHA_MSG_LEFT_3", 0x1f008f220, "uint32_t"),
    ("SE_CRYPTO_SECURITY_PERKEY", 0x1f008f280, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_0", 0x1f008f284, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_1", 0x1f008f288, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_2", 0x1f008f28c, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_3", 0x1f008f290, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_4", 0x1f008f294, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_5", 0x1f008f298, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_6", 0x1f008f29c, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_7", 0x1f008f2a0, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_8", 0x1f008f2a4, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_9", 0x1f008f2a8, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_10", 0x1f008f2ac, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_11", 0x1f008f2b0, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_12", 0x1f008f2b4, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_13", 0x1f008f2b8, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_14", 0x1f008f2bc, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ACCESS_15", 0x1f008f2c0, "uint32_t"),
    ("SE_CRYPTO_CONFIG", 0x1f008f304, "uint32_t"),
    ("SE_CRYPTO_LINEAR_CTR_0", 0x1f008f308, "uint32_t"),
    ("SE_CRYPTO_LINEAR_CTR_1", 0x1f008f30c, "uint32_t"),
    ("SE_CRYPTO_LINEAR_CTR_2", 0x1f008f310, "uint32_t"),
    ("SE_CRYPTO_LINEAR_CTR_3", 0x1f008f314, "uint32_t"),
    ("SE_CRYPTO_LAST_BLOCK", 0x1f008f318, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_ADDR", 0x1f008f31c, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_DATA", 0x1f008f320, "uint32_t"),
    ("SE_CRYPTO_KEYTABLE_DST", 0x1f008f330, "uint32_t"),
    ("SE_RNG_CONFIG", 0x1f008f340, "uint32_t"),
    ("SE_RNG_SRC_CONFIG", 0x1f008f344, "uint32_t"),
    ("SE_RNG_RESEED_INTERVAL", 0x1f008f348, "uint32_t"),
    ("SE_RSA_CONFIG", 0x1f008f400, "uint32_t"),
    ("SE_RSA_KEY_SIZE", 0x1f008f404, "uint32_t"),
    ("SE_RSA_EXP_SIZE", 0x1f008f408, "uint32_t"),
    ("SE_RSA_SECURITY_PERKEY", 0x1f008f40c, "uint32_t"),
    ("SE_RSA_KEYTABLE_ACCESS_0", 0x1f008f410, "uint32_t"),
    ("SE_RSA_KEYTABLE_ACCESS_1", 0x1f008f414, "uint32_t"),
    ("SE_RSA_KEYTABLE_ADDR", 0x1f008f420, "uint32_t"),
    ("SE_RSA_KEYTABLE_DATA", 0x1f008f424, "uint32_t"),
    ("SE_RSA_OUTPUT_0", 0x1f008f428, "uint32_t"),
    ("SE_RSA_OUTPUT_1", 0x1f008f42c, "uint32_t"),
    ("SE_RSA_OUTPUT_2", 0x1f008f430, "uint32_t"),
    ("SE_RSA_OUTPUT_3", 0x1f008f434, "uint32_t"),
    ("SE_RSA_OUTPUT_4", 0x1f008f438, "uint32_t"),
    ("SE_RSA_OUTPUT_5", 0x1f008f43c, "uint32_t"),
    ("SE_RSA_OUTPUT_6", 0x1f008f440, "uint32_t"),
    ("SE_RSA_OUTPUT_7", 0x1f008f444, "uint32_t"),
    ("SE_RSA_OUTPUT_8", 0x1f008f448, "uint32_t"),
    ("SE_RSA_OUTPUT_9", 0x1f008f44c, "uint32_t"),
    ("SE_RSA_OUTPUT_10", 0x1f008f450, "uint32_t"),
    ("SE_RSA_OUTPUT_11", 0x1f008f454, "uint32_t"),
    ("SE_RSA_OUTPUT_12", 0x1f008f458, "uint32_t"),
    ("SE_RSA_OUTPUT_13", 0x1f008f45c, "uint32_t"),
    ("SE_RSA_OUTPUT_14", 0x1f008f460, "uint32_t"),
    ("SE_RSA_OUTPUT_15", 0x1f008f464, "uint32_t"),
    ("SE_RSA_OUTPUT_16", 0x1f008f468, "uint32_t"),
    ("SE_RSA_OUTPUT_17", 0x1f008f46c, "uint32_t"),
    ("SE_RSA_OUTPUT_18", 0x1f008f470, "uint32_t"),
    ("SE_RSA_OUTPUT_19", 0x1f008f474, "uint32_t"),
    ("SE_RSA_OUTPUT_20", 0x1f008f478, "uint32_t"),
    ("SE_RSA_OUTPUT_21", 0x1f008f47c, "uint32_t"),
    ("SE_RSA_OUTPUT_22", 0x1f008f480, "uint32_t"),
    ("SE_RSA_OUTPUT_23", 0x1f008f484, "uint32_t"),
    ("SE_RSA_OUTPUT_24", 0x1f008f488, "uint32_t"),
    ("SE_RSA_OUTPUT_25", 0x1f008f48c, "uint32_t"),
    ("SE_RSA_OUTPUT_26", 0x1f008f490, "uint32_t"),
    ("SE_RSA_OUTPUT_27", 0x1f008f494, "uint32_t"),
    ("SE_RSA_OUTPUT_28", 0x1f008f498, "uint32_t"),
    ("SE_RSA_OUTPUT_29", 0x1f008f49c, "uint32_t"),
    ("SE_RSA_OUTPUT_30", 0x1f008f4a0, "uint32_t"),
    ("SE_RSA_OUTPUT_31", 0x1f008f4a4, "uint32_t"),
    ("SE_RSA_OUTPUT_32", 0x1f008f4a8, "uint32_t"),
    ("SE_RSA_OUTPUT_33", 0x1f008f4ac, "uint32_t"),
    ("SE_RSA_OUTPUT_34", 0x1f008f4b0, "uint32_t"),
    ("SE_RSA_OUTPUT_35", 0x1f008f4b4, "uint32_t"),
    ("SE_RSA_OUTPUT_36", 0x1f008f4b8, "uint32_t"),
    ("SE_RSA_OUTPUT_37", 0x1f008f4bc, "uint32_t"),
    ("SE_RSA_OUTPUT_38", 0x1f008f4c0, "uint32_t"),
    ("SE_RSA_OUTPUT_39", 0x1f008f4c4, "uint32_t"),
    ("SE_RSA_OUTPUT_40", 0x1f008f4c8, "uint32_t"),
    ("SE_RSA_OUTPUT_41", 0x1f008f4cc, "uint32_t"),
    ("SE_RSA_OUTPUT_42", 0x1f008f4d0, "uint32_t"),
    ("SE_RSA_OUTPUT_43", 0x1f008f4d4, "uint32_t"),
    ("SE_RSA_OUTPUT_44", 0x1f008f4d8, "uint32_t"),
    ("SE_RSA_OUTPUT_45", 0x1f008f4dc, "uint32_t"),
    ("SE_RSA_OUTPUT_46", 0x1f008f4e0, "uint32_t"),
    ("SE_RSA_OUTPUT_47", 0x1f008f4e4, "uint32_t"),
    ("SE_RSA_OUTPUT_48", 0x1f008f4e8, "uint32_t"),
    ("SE_RSA_OUTPUT_49", 0x1f008f4ec, "uint32_t"),
    ("SE_RSA_OUTPUT_50", 0x1f008f4f0, "uint32_t"),
    ("SE_RSA_OUTPUT_51", 0x1f008f4f4, "uint32_t"),
    ("SE_RSA_OUTPUT_52", 0x1f008f4f8, "uint32_t"),
    ("SE_RSA_OUTPUT_53", 0x1f008f4fc, "uint32_t"),
    ("SE_RSA_OUTPUT_54", 0x1f008f500, "uint32_t"),
    ("SE_RSA_OUTPUT_55", 0x1f008f504, "uint32_t"),
    ("SE_RSA_OUTPUT_56", 0x1f008f508, "uint32_t"),
    ("SE_RSA_OUTPUT_57", 0x1f008f50c, "uint32_t"),
    ("SE_RSA_OUTPUT_58", 0x1f008f510, "uint32_t"),
    ("SE_RSA_OUTPUT_59", 0x1f008f514, "uint32_t"),
    ("SE_RSA_OUTPUT_60", 0x1f008f518, "uint32_t"),
    ("SE_RSA_OUTPUT_61", 0x1f008f51c, "uint32_t"),
    ("SE_RSA_OUTPUT_62", 0x1f008f520, "uint32_t"),
    ("SE_RSA_OUTPUT_63", 0x1f008f524, "uint32_t"),
    ("SE_STATUS", 0x1f008f800, "uint32_t"),
    ("SE_ERR_STATUS", 0x1f008f804, "uint32_t"),
    ("SE_MISC", 0x1f008f808, "uint32_t"),
    ("SE_SPARE", 0x1f008f80c, "uint32_t"),
    ("SE_ENTROPY_DEBUG_COUNTER", 0x1f008f810, "uint32_t"),
]

STRUCTS = [
    """
struct PushedRegisters __packed
{
    uint64_t x0;
    uint64_t x1;
    uint64_t x2;
    uint64_t x3;
    uint64_t x4;
    uint64_t x5;
    uint64_t x6;
    uint64_t x7;
    uint64_t x8;
    uint64_t x9;
    uint64_t x10;
    uint64_t x11;
    uint64_t x12;
    uint64_t x13;
    uint64_t x14;
    uint64_t x15;
    uint64_t x16;
    uint64_t x17;
    uint64_t x18;
    uint64_t x19;
    uint64_t x29;
    uint64_t x30;
};
""",

    """
struct SmcHandler __packed
{
    uint64_t sub_id;
    uint32_t (* handler)(struct PushedRegisters* arguments);
};
""",

    """
struct SmcHandlerSectionInfo __packed
{
    struct SmcHandler* handler_array;
    uint64_t handler_count;
};
""",
    """
struct Package2Header __packed
{
    uint8_t header_ctr[0x10];
    uint8_t section_ctrs[4][0x10];
    uint32_t magic;
    uint32_t base_offset;
    uint32_t res;
    uint8_t package2_version;
    uint8_t bootloader_version;
    uint16_t padding;
    uint32_t section_sizes[4];
    uint32_t section_offsets[4];
    uint8_t section_hashes[4][0x20];
};
""",
    """
struct RsaCtx __packed {
	uint32_t key_size_div_64_minus_1;
	uint32_t exponent_size_div_4;
};
""",
    """
struct SeLLEntry __packed {
    uint32_t idx;
    uint32_t translated_address;
    uint32_t size;
};
""",
    """
struct CARDeviceDefinition __packed {
    uint64_t reset_reg_off;
    uint64_t enable_reg_off;
    uint64_t source_reg_off;
    uint8_t register_bitshift;
    uint8_t clock_src;
    uint8_t clock_div;
    uint8_t res0;
    uint32_t res1;
};
"""
]


class SecmonView(BinaryView):
    #old_name = "Secmon (3.0.2)"
    name = "NX Secure Monitor (3.0.2)"

    def log(self, msg, error=False):
        msg = f"[NX-Secure-Monitor-Loader] {msg}"
        if not error:
            log_info(msg)
        else:
            log_error(msg)

    def __init__(self, data):
        self.raw = data
        self.reader = BinaryReader(data, Endianness.LittleEndian)
        self.writer = BinaryWriter(data, Endianness.LittleEndian)
        BinaryView.__init__(self, file_metadata=data.file, parent_view=data)

    @classmethod
    def is_valid_for_data(cls, data):
        return data.read(0x9630, 5) == b"OHAYO"

    def define_function(self, name: str, return_value: str, args: list[str], address: int, noreturn: bool) -> bool:
        self.log(f"Adding {name} @ {hex(address)}")
        self.add_function(address, self.platform)
        self.define_user_symbol(
            Symbol(SymbolType.FunctionSymbol, address, name))

        f = self.get_function_at(address)
        if f == None:
            # A race condition was introduced in a recent dev build of Binary Ninja...
            self.log(
                "Race condition was triggered :c try restarting binary ninja and reloading...", error=True)
            return False
        f.name = name
        args_str = ", ".join(args)
        noreturn_str = ""
        if noreturn:
            noreturn_str = " __noreturn"
        ty = f"{return_value} {name}({args_str}){noreturn_str}"
        f.apply_auto_discovered_type(
            self.parse_type_string(ty)[0])
        return True

    def parse_smc_handler_table(self, smc_id: int, table_start: int, table_size: int):
        for x in range(1, table_size):
            address = table_start + (x * 0x10)
            handler_id, function_address = self.read_int(
                address, 8, False, self.endianness), self.read_int(address + 8, 8, False, self.endianness)
            name = "smc" + str(smc_id) + "_handler_" + hex(handler_id)[2:]
            self.define_function(name, "uint32_t", [
                                 "PushedRegisters *arguments"], function_address, False)

    def parse_smc_handler_info_table(self, metadata_addr: int):
        self.define_user_data_var(
            metadata_addr, "struct SmcHandlerSectionInfo[0x2]", "smc_handler_section_info")
        smc0_handler_table_address, smc0_handler_table_size = self.read_int(
            metadata_addr, 8, False, self.endianness), self.read_int(metadata_addr + 8, 8, False, self.endianness)
        smc1_handler_table_address, smc1_handler_table_size = self.read_int(
            metadata_addr + 0x10, 8, False, self.endianness), self.read_int(metadata_addr + 0x18, 8, False, self.endianness)
        self.define_user_data_var(smc0_handler_table_address, "struct SmcHandler[" + hex(
            smc0_handler_table_size) + "]", "smc0_handlers")
        self.define_user_data_var(smc1_handler_table_address, "struct SmcHandler[" + hex(
            smc1_handler_table_size) + "]", "smc1_handlers")

        self.parse_smc_handler_table(
            0, smc0_handler_table_address, smc0_handler_table_size)
        self.parse_smc_handler_table(
            1, smc1_handler_table_address, smc1_handler_table_size)

    def on_complete(self):
        for (name, addr, ty) in DATA:
            existing = self.get_data_var_at(addr)
            if existing != None:
                existing.type = self.parse_type_string(ty)[0]
                existing.name = name
            else:
                self.define_user_data_var(addr, ty, name)

        for (name, addr, ty) in REGISTERS:
            existing = self.get_data_var_at(addr)
            if existing != None:
                existing.type = self.parse_type_string(ty)[0]
                existing.name = name
            else:
                self.define_user_data_var(addr, ty, name)

    def init(self):
        self.arch = Architecture['aarch64']
        self.platform = Architecture["aarch64"].standalone_platform

        self.log("Loading NX-Secure-Monitor for FW 3.0.2")

        self.add_auto_segment(BASE, len(self.raw), 0, len(self.raw), SegmentFlag.SegmentExecutable | SegmentFlag.SegmentReadable |
                              SegmentFlag.SegmentWritable | SegmentFlag.SegmentContainsCode | SegmentFlag.SegmentContainsData)
        self.add_auto_section(".text", BASE, len(
            self.raw), SectionSemantics.ReadOnlyCodeSectionSemantics)

        for x in MAPPINGS:
            self.add_user_segment(x[1], x[2], 0, 0, x[3])
            self.add_user_section(x[0], x[1], x[2])

        for (name, source_start, source_len, va_dest_start, va_dest_len, flags) in VIRTUAL_MAPPINGS["code"]:
            self.add_user_segment(va_dest_start, va_dest_len,
                                  source_start - BASE, source_len, flags)
            self.add_user_section(name, va_dest_start, va_dest_len)

        for (name, source_start, source_len, va_dest_start, va_dest_len, flags) in VIRTUAL_MAPPINGS["mmio"]:
            name = f"V_{name}"
            self.add_user_segment(va_dest_start, va_dest_len,
                                  source_start - BASE, source_len, flags)
            self.add_user_section(name, va_dest_start, va_dest_len)
            self.define_user_symbol(
                Symbol(SymbolType.DataSymbol, va_dest_start, name))

        for (name, addr, length) in VIRTUAL_MAPPINGS["data"]:
            name = f"V_{name}"
            self.add_user_segment(
                addr, length, 0, 0, SegmentFlag.SegmentReadable | SegmentFlag.SegmentWritable)
            self.add_user_section(name, addr, length)
            self.define_user_symbol(Symbol(SymbolType.DataSymbol, addr, name))

        for val in STRUCTS:
            ty, name = self.parse_type_string(val)
            self.log(f"Adding struct {name}")
            self.define_user_type(name, ty)

        self.add_entry_point(BASE)

        for (name, vaddr, metadata) in FUNCTIONS:
            noreturn = False
            if len(metadata) == 3:
                noreturn = metadata[2]
            if self.define_function(
                    name, metadata[0], metadata[1], vaddr, noreturn) == False:
                return False

        self.parse_smc_handler_info_table(0x1f01e7070)

        self.update_analysis_and_wait()

        AnalysisCompletionEvent(self, self.on_complete)

        return True
